<?php
/**
 * ip-hamburg functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ip-hamburg
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'iph_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function iph_setup() {

		// Remove unnecessary code from WP frontend

		remove_action ('wp_head', 'rsd_link');
		remove_action( 'wp_head', 'wlwmanifest_link');
		remove_action( 'wp_head', 'wp_shortlink_wp_head');
		remove_action('wp_head', 'rest_output_link_wp_head', 10);
		remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
		remove_action('template_redirect', 'rest_output_link_header', 11, 0);
		remove_action( 'wp_head', 'feed_links', 2 ); //removes feed links.
		remove_action('wp_head', 'feed_links_extra', 3 );  //removes comments feed.
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ip-hamburg, use a find and replace
		 * to change 'iph' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'iph', get_template_directory() . '/languages' );


		// This theme uses wp_nav_menu() in one location.
		//register nav
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'iph' ),
			'menu-2' => esc_html__( 'Secondary', 'iph' ),
			'menu-3' => esc_html__( 'Footer', 'iph' ),
		) );

		add_image_size( 'iph-size-xl', 1700 );
		add_image_size( 'iph-size-l', 1200 );
		add_image_size( 'iph-size-m', 700 );
		add_image_size( 'iph-size-s', 600 );
		add_image_size( 'iph-size-backend-l', 500 );
		add_image_size( 'iph-size-backend-m', 300 );
		add_image_size( 'iph-size-backend-s', 150 );
		add_image_size( 'iph-project-big', 300, 300, true );
		add_image_size( 'iph-masonry-q', 400, 290, true );
		add_image_size( 'iph-masonry-qg', 800, 580, true );
		add_image_size( 'iph-masonry-qs', 800, 290, true );
		add_image_size( 'iph-masonry-h', 400, 580, true );
		add_image_size( 'iph-timeline', 300, 200, true );
		add_image_size( 'iph-project-list', 600, 400, true );
		add_image_size( 'iph-project-detail', 450, 300, true );

		//Bootstrap navwalker
		require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';




	}
endif;
add_action( 'after_setup_theme', 'iph_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iph_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'iph_content_width', 640 );
}
add_action( 'after_setup_theme', 'iph_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iph_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'iph' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'iph' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'iph_widgets_init' );


//css
function iph_scripts() {

	wp_enqueue_style( 'iph-slick',  get_template_directory_uri() . '/lib/slick/slick.css' );
	wp_enqueue_style( 'iph-slick-theme',  get_template_directory_uri() . '/lib/slick/slick-theme.css' );
	wp_enqueue_style( 'iph-main',  get_template_directory_uri() . '/css/style.min.css' );

}
add_action( 'wp_enqueue_scripts', 'iph_scripts' );

//register js
function iph_register_js() {



	  wp_enqueue_script( 'iph-slick', get_template_directory_uri() . '/lib/slick/slick.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'iph-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'iph-lazysizes-bgset', get_template_directory_uri() . '/js/ls.bgset.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'iph-lazysizes', get_template_directory_uri() . '/js/lazysizes.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'iph-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_script( 'iph-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array ( 'jquery' ), 1.1, true);

    wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ),'1.0.0', true );
}

//enque js
if ( !is_admin() ) {
    add_action('wp_enqueue_scripts', 'iph_register_js');
}



function iph_rename_post_menu() {
	global $menu;
	$menu[5][0] = 'Aktuelles'; // Änderung von "Beiträge" in "Kunden
	}
add_action( 'admin_menu', 'iph_rename_post_menu' );



// Register Custom Post Type
function iph_projects_post_type() {

	$labels = array(
		'name'                  => _x( 'Projekte', 'Post Type General Name', 'iph' ),
		'singular_name'         => _x( 'Projekt', 'Post Type Singular Name', 'iph' ),
		'menu_name'             => __( 'Projekte', 'iph' ),
		'name_admin_bar'        => __( 'Projekte', 'iph' ),
		'archives'              => __( 'Archiv', 'iph' ),
		'attributes'            => __( 'Attribute', 'iph' ),
		'parent_item_colon'     => __( 'Eltern Element', 'iph' ),
		'all_items'             => __( 'Alle Projekte', 'iph' ),
		'add_new_item'          => __( 'Neues Projekt', 'iph' ),
		'add_new'               => __( 'Neues Projekt', 'iph' ),
		'new_item'              => __( 'Neues Projekt', 'iph' ),
		'edit_item'             => __( 'Bearbeiten', 'iph' ),
		'update_item'           => __( 'Aktualiseren', 'iph' ),
		'view_item'             => __( 'Ansehen', 'iph' ),
		'view_items'            => __( 'Ansehen', 'iph' ),
		'search_items'          => __( 'Suchen', 'iph' ),
		'not_found'             => __( 'Keine Projekte vorhanden', 'iph' ),
		'not_found_in_trash'    => __( 'Keine Projekte vorhanden', 'iph' ),
		'featured_image'        => __( 'Featured Image', 'iph' ),
		'set_featured_image'    => __( 'Set featured image', 'iph' ),
		'remove_featured_image' => __( 'Remove featured image', 'iph' ),
		'use_featured_image'    => __( 'Use as featured image', 'iph' ),
		'insert_into_item'      => __( 'Einsetzen', 'iph' ),
		'uploaded_to_this_item' => __( 'Zu diesem Projekt hochgeladen', 'iph' ),
		'items_list'            => __( 'Projektliste', 'iph' ),
		'items_list_navigation' => __( 'Navigation', 'iph' ),
		'filter_items_list'     => __( 'Filter', 'iph' ),
	);
	$rewrite = array(
		'slug'                  => 'projekte',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Projekt', 'iph' ),
		'description'           => __( 'Post Type Description', 'iph' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'projects', $args );

}
add_action( 'init', 'iph_projects_post_type', 0 );



// Register Custom Post Type
function iph_timeline_post_type() {

	$labels = array(
		'name'                  => _x( 'Chronik', 'Post Type General Name', 'iph' ),
		'singular_name'         => _x( 'Chronik', 'Post Type Singular Name', 'iph' ),
		'menu_name'             => __( 'Chronik', 'iph' ),
		'name_admin_bar'        => __( 'Chronik', 'iph' ),
		'archives'              => __( 'Archiv', 'iph' ),
		'attributes'            => __( 'Attribute', 'iph' ),
		'parent_item_colon'     => __( 'Eltern Element', 'iph' ),
		'all_items'             => __( 'Alle Einträge', 'iph' ),
		'add_new_item'          => __( 'Neuer Eintrag', 'iph' ),
		'add_new'               => __( 'Neuer Eintrag', 'iph' ),
		'new_item'              => __( 'Neues Eintrag', 'iph' ),
		'edit_item'             => __( 'Bearbeiten', 'iph' ),
		'update_item'           => __( 'Aktualiseren', 'iph' ),
		'view_item'             => __( 'Ansehen', 'iph' ),
		'view_items'            => __( 'Ansehen', 'iph' ),
		'search_items'          => __( 'Suchen', 'iph' ),
		'not_found'             => __( 'Keine Einträge vorhanden', 'iph' ),
		'not_found_in_trash'    => __( 'Keine Einträge vorhanden', 'iph' ),
		'featured_image'        => __( 'Featured Image', 'iph' ),
		'set_featured_image'    => __( 'Set featured image', 'iph' ),
		'remove_featured_image' => __( 'Remove featured image', 'iph' ),
		'use_featured_image'    => __( 'Use as featured image', 'iph' ),
		'insert_into_item'      => __( 'Einsetzen', 'iph' ),
		'uploaded_to_this_item' => __( 'Zu diesem Projekt hochgeladen', 'iph' ),
		'items_list'            => __( 'Chronik', 'iph' ),
		'items_list_navigation' => __( 'Navigation', 'iph' ),
		'filter_items_list'     => __( 'Filter', 'iph' ),
	);
	$rewrite = array(
		'slug'                  => 'timeline',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Chronik', 'iph' ),
		'description'           => __( 'Alle Ereignisse', 'iph' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-backup',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'timeline', $args );

}
add_action( 'init', 'iph_timeline_post_type', 0 );



// Register Custom Post Type
function iph_team_post_type() {

	$labels = array(
		'name'                  => _x( 'Team', 'Post Type General Name', 'iph' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'iph' ),
		'menu_name'             => __( 'Team', 'iph' ),
		'name_admin_bar'        => __( 'Team', 'iph' ),
		'archives'              => __( 'Archiv', 'iph' ),
		'attributes'            => __( 'Attribute', 'iph' ),
		'parent_item_colon'     => __( 'Eltern Element', 'iph' ),
		'all_items'             => __( 'Alle Einträge', 'iph' ),
		'add_new_item'          => __( 'Neuer Eintrag', 'iph' ),
		'add_new'               => __( 'Neuer Eintrag', 'iph' ),
		'new_item'              => __( 'Neues Eintrag', 'iph' ),
		'edit_item'             => __( 'Bearbeiten', 'iph' ),
		'update_item'           => __( 'Aktualiseren', 'iph' ),
		'view_item'             => __( 'Ansehen', 'iph' ),
		'view_items'            => __( 'Ansehen', 'iph' ),
		'search_items'          => __( 'Suchen', 'iph' ),
		'not_found'             => __( 'Keine Einträge vorhanden', 'iph' ),
		'not_found_in_trash'    => __( 'Keine Einträge vorhanden', 'iph' ),
		'featured_image'        => __( 'Featured Image', 'iph' ),
		'set_featured_image'    => __( 'Set featured image', 'iph' ),
		'remove_featured_image' => __( 'Remove featured image', 'iph' ),
		'use_featured_image'    => __( 'Use as featured image', 'iph' ),
		'insert_into_item'      => __( 'Einsetzen', 'iph' ),
		'uploaded_to_this_item' => __( 'Zu diesem Projekt hochgeladen', 'iph' ),
		'items_list'            => __( 'Team', 'iph' ),
		'items_list_navigation' => __( 'Navigation', 'iph' ),
		'filter_items_list'     => __( 'Filter', 'iph' ),
	);
	$rewrite = array(
		'slug'                  => 'team',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Team', 'iph' ),
		'description'           => __( 'Teammitglieder', 'iph' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'iph_team_post_type', 0 );




/////////////////////////////////////////////////////////////////
// ACF
//////////////////////////////////////////////////////////////////

//add acf options page
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page('Theme Optionen');

}



//set layout title in backend
add_filter('acf/fields/flexible_content/layout_title/name=blocks', 'iph_acf_fields_flexible_content_layout_title', 10, 4);
function iph_acf_fields_flexible_content_layout_title( $title, $field, $layout, $i ) {

    // Remove layout name from title.
    $old_title = $title;
		//$title = '';


    // load text sub field
    if( $section_title = get_sub_field('section_title') ) {
        $title .= ' - <b>' . esc_html($section_title) . '</b>';
    }
    return $title;
}
