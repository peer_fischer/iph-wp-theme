 <?php
/*
 * Template Name: Projektliste
 * Description: Projektliste
 */



get_header(); ?>





  <?php

  $category = get_category( get_query_var( 'cat' ) );
  $cat_id = $category->cat_ID;
  $headline = get_the_title();

  echo $cat_id;

  $o .='<!-- SECTION PROJECTS // START-->';
  $o .='<section class="s-projects" id="" style="">';
  $o .= '<div class="container">';

  $o .= '<div class="row pt-4 pb-4"><div class="col-12"><h2>'.$headline.'</h2></div></div>';
  $o.= '<div class="row"><div class="col-12"><div class="row no-margin">';


  // WP_Query arguments
  $args = array(
  	'post_type'              => array( 'projects' ),
  	'post_status'            => array( 'publish' ),
  	'posts_per_page'         => '100',
    'order' => 'ASC',
    'orderby' => 'menu_order',

  );

  // The Query
  $query = new WP_Query( $args );

  // The Loop
  if ( $query->have_posts() ) {
  	while ( $query->have_posts() ) {
  		$query->the_post();

      $img = get_field('project_teaser_img');
      $alt = $img['alt'];
      $size = 'iph-project-list';
      $img_url = wp_get_attachment_image_url( $img['id'], $size );
      $title= get_field('project_titel');
      $subtitle = get_field('project_subtitel');
      $link = get_permalink();



      $o .= '<div class="col-12 col-md-6 col-lg-4 no-padding project"><a href="'.$link.'">';

      if (!empty($title)) {
              $o .= ' <div class="info-wrapper"><div class="info"><span class="title d-block w-100">'.$title.'</span><span class="subtitle d-block w-100">'.$subtitle.'</span></div></div>';
  }
        $o .= '<img class="img-fluid lazyload" src="'.$img_url.'" alt="'.$alt.'" />

            </a></div>';

  	}
  } else {
  	$o .= 'Keine Projekte vorhanden.';
  }

  // Restore original Post Data
  wp_reset_postdata();







  $o .='</div></div></div></div></section>';
  $o.='<!-- SECTION PROJECTS // END-->';

  echo $o;


?>





<?php get_footer(); ?>
