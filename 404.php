<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ip-hamburg
 */

get_header();
?>

	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1>404</h1>
				<h2>Seite nicht gefunden</h2>
			</div>
		</div>
	</div>

<?php
get_footer();
