 <?php
/*
 * Template Name: Startseite
 * Description: Startseite
 */



get_header(); ?>


<div class="container">
<div class="row pt-4 pb-4"><div class="col-8 col-md-8"><h2>Projekte</h2></div><div class="col-4 col-md-4 ">
<a href="<?php echo get_permalink(54); ?>" class="btn-primary float-right d-none d-sm-block mr-1">Alle Projekte</a></div></div>
</div>

<div class="container-fluid">

<div class="row">

<div class="col-12">


<div class="grid">
<div class="grid-sizer"></div>
<?php


$o = '';
$i = '';
$addclass = '';

// Check rows exists.
if( have_rows('project_teasers') ) {

    // Loop through rows.
    while( have_rows('project_teasers') ) { the_row();

      $img = get_sub_field('project_teaser_img');
      $alt = $img['alt'];
      $dimensions = $img['width'].$img['height'];
      $format = get_sub_field('project_teaser_img_format');
      $title= get_sub_field('project_teaser_title');
      $subtitle= get_sub_field('project_teaser_subtitle');
      $i++;

      switch ($format) {
        case 'q':
            $width = 'width1';
            $size = 'iph-masonry-q';
            break;
        case 'qg':
            $width = 'width2';
            $size = 'iph-masonry-qg';
            break;
        case 'qs':
            $width = 'width2';
            $size = 'iph-masonry-qs';
            break;
        case 'h':
            $width = 'width1';
            $size = 'iph-masonry-h';
            break;
    }

     if ($i>4) {
       $addclass = 'd-none d-sm-block';
     }


      $img_url = wp_get_attachment_image_url( $img['id'], $size );

      $o .= '<div class="grid-item '.$addclass.' grid-item--'.$width.'">';
      $o .= '<div class="item-border">';
      $o .= '<a href="#">';
      if (!empty($title)) {
        $o .= '<div class="info-wrapper"><div class="info"><span class="title d-block w-100">'.$title.'</span><span class="subtitle d-block w-100">'.$subtitle.'</span></div></div>';
      }
      $o .= '<img src="'.$img_url.'" alt="'.$alt.'" />';
      $o .= '</a>';
      $o .= '</div>';
      $o .= '</div>';




    }
    echo $o;
}



?>
</div>
</div>
</div>
</div>
<div class="container">
<div class="row"><div class="col-12">
<a href="<?php echo get_permalink(54); ?>" class="btn-primary mb-5 d-block d-sm-none">Alle Projekte</a></div>
</div></div>



<?php include ('inc/page-builder.php'); ?>


<?php get_footer(); ?>
