<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ip-hamburg
 */

get_header();
?>

<section>
<div class="container">
	<div class="row pt-3 pb-4" >
		<div class="col-12">
			<h1>	<?php

				echo get_the_title();

				 ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-md-6 project-images mb-3">
     <div class="row no-margin">


			<?php


			$o = '';
			$i = '';
			$addclass = '';

			// Check rows exists.
			if( have_rows('project_detail_images') ) {

			    // Loop through rows.
			    while( have_rows('project_detail_images') ) { the_row();

			      $img = get_sub_field('project_detail_img');
			      $alt = $img['alt'];
			      $dimensions = $img['width'].$img['height'];
						$size = 'iph-project-detail';
						$img_url = wp_get_attachment_image_url( $img['id'], $size );


			      $o .= '<div class="col-md-6 no-padding"><img class="img-fluid w-100 lazyload" src="'.$img_url.'" alt="'.$alt.'" /></div>';





			    }
			    echo $o;
			}



			?>
		</div>
		</div>
		<div class="col-12 col-md-5 offset-md-1 project-text pt-5">
			<?php the_field('project_detail_description'); ?>
			<h2 class="mt-5">Planungsumfang, der von der industrie planung gmbh geleistet wurde:</h2>
			<?php the_field('project_detail_job'); ?>
		</div>
	</div>
</div>

</section>



	<?php

	global $post;
	$postcat = get_the_category( $post->ID );
	$term_id =  $postcat[0]->cat_ID;
	$headline =  $postcat[0]->cat_name;

  $o = '';
	$o .='<!-- SECTION PROJECTS // START-->';
	$o .='<section class="s-projects" id="" style="">';
	$o .= '<div class="container">';

	$o .= '<div class="row pb-4 pt-4"><div class="col-12 col-md-8"><h2>'.$headline.'</h2></div><div class="d-none d-md-block col-md-4 ">';
  $o.= '<a href="'.$btn_link.'" class="btn-primary float-right">Alle Projekte</a>';



	$o.= '</div></div><div class="row"><div class="bg-50-left">

	</div><div class="col-12"><div class="row no-margin">';


	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'projects' ),
		'post_status'            => array( 'publish' ),
		'posts_per_page'         => '3',
		'cat' => $term_id,
		'order' => 'ASC',
		'orderby' => 'menu_order',

	);

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();

			$img = get_field('project_teaser_img');
			$alt = $img['alt'];
			$size = 'iph-project-list';
			$img_url = wp_get_attachment_image_url( $img['id'], $size );
			$title= get_field('project_titel');
			$subtitle = get_field('project_subtitel');
			$link = get_permalink();



			$o .= '<div class="col-12 col-md-6 col-lg-4 no-padding project"><a href="'.$link.'">';

			if (!empty($title)) {
							$o .= ' <div class="info-wrapper"><div class="info"><span class="title d-block w-100">'.$title.'</span><span class="subtitle d-block w-100">'.$subtitle.'</span></div></div>';
	}
				$o .= '<img class="img-fluid lazyload" src="'.$img_url.'" alt="'.$alt.'" />

						</a></div>';



		}
	} else {
		$o .= 'Keine Projekte vorhanden.';
	}

	// Restore original Post Data
	wp_reset_postdata();







	$o .='</div>';
	 $o.= '<a href="'.$btn_link.'" class="btn-primary d-block d-md-none mt-3">Alle Projekte</a>';
	$o .='</div></div></div></section>';
	$o.='<!-- SECTION PROJECTS // END-->';

	echo $o;

	 ?>




<?php

get_footer();
