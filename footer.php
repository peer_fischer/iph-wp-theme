<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ip-hamburg
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="container">
      <div class="row">


			<div class="col-12 col-lg-4">
			<a  href="<?php echo home_url(); ?>"><img class="lazyload" src="<?php echo get_template_directory_uri(); ?>/img/ip_logo.svg" alt="IP Hamburg"></a>
			</div>
			<div class="col-12 col-lg-3 pt-3 pl-lg-5">
			<?php the_field('footer_address','option'); ?>
			</div>
			<div class="col-12 col-lg-3 d-flex">
				<nav class="align-self-end pb-3" >
					<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-3',
					'menu_id'        => 'footer-menu',
				) );
				?>
				 </nav>
			</div>
			<div class="col-12 col-lg-2 d-flex pb-3">
				<span class="align-self-end">© <?php echo date("Y"); ?> ip-hamburg.com</span>
			</div>

	</div>
		</div>

	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
