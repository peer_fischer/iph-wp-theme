


jQuery(document).ready(function ($) {


  jQuery('ul.nav li.dropdown').hover(function() {

    jQuery(this).find('.dropdown-toggle').addClass('open');
  }, function() {

    jQuery(this).find('.dropdown-toggle').removeClass('open');
  });

  if ($("#timeline").length) {
    $('#timeline').slick({
      dots: true,
      autoplaySpeed: 2000,
      infinite: false,
      speed: 900,
      variableWidth:true,
      slidesToScroll: 3,
      touchThreshold: 20,
      pauseOnHover: false,
      prevArrow: false,
      nextArrow: false,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            draggable: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            swipe: true,
            draggable: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true,
            draggable: true
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });
  }

    if ( $( ".grid" ).length ) {

      var $grid = $(".grid").imagesLoaded(function() {

         $grid.masonry({
           itemSelector: ".grid-item"
         });
       });

       $(".grid").imagesLoaded()
         .progress(function(instance, image) {
           var $item = $(image.img);

           $item.css({
             "opacity": "1",
             "transition-delay": Math.random() + "s"
           });
         });

    }






});
