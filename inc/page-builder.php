<?php

ob_start();

$id = get_the_ID();
$flex_field_array = get_post_meta($id, 'blocks', true);
//print_r($flex_field_array);

$count = 0;
if (is_array($flex_field_array)) {
  $count = count($flex_field_array);
}
echo '<!-- found '.$count.' blocks-->';

// check if the flexible content field has rows of data
if ( have_rows( 'blocks', $id ) ) :

    $i ='';

    // loop through the selected ACF layouts and display the matching partial
    while ( have_rows( 'blocks') ) : the_row();


    $i++;

        get_template_part('inc/partials/' . get_row_layout() , 'my-template',
    array(
        'section-id' => $i, // passing this array possible since WP 5.5
    ));




    endwhile;


endif;


?>
