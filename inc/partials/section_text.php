<?php

$o ='';
$inline_css = '';
$btn_text = '';



$headline = get_sub_field('section_text_headline');
$headline_markup = get_sub_field('section_text_headline_markup');
$bg = get_sub_field('section_text_bg');
$cols = get_sub_field('section_text_cols');
$btn_text = get_sub_field('section_text_btn_text');
$btn_link = get_sub_field('section_text_btn_link');
$content = get_sub_field('section_text_content');
$bg = get_sub_field('section_img_text_img_bg');


$o .='<!-- SECTION TEXT // START-->';
$o .='<section class="s-text" id="" style="">';
$o .= '<div class="container">';


$o .= '<div class="row pt-4 pb-2"><div class="col-8"><'.$headline_markup.'>'.$headline.'</'.$headline_markup.'></div><div class="col-4 ">';

if (!empty($btn_text)) {

  $o.= '<a href="'.$btn_link.'" class="btn-primary float-right">'.$btn_text.'</a>';

}

$o.= '</div></div>';

$o .= '<div class="row">
         <div class="col-12 col-md-'.$cols.' section-content">'.$content.'</div>';


$o .='</div></section>';
$o.='<!-- SECTION TEXT // END-->';


echo $o;

?>
