<?php

$o ='';
$inline_css = '';
$btn_text = '';







$o .='<!-- SECTION TIMELINE // START-->';
$o .='<section class="s-timeline" id="" style="">';
$o .= '<div class="container">';
$o .= '<div class="row pt-3 pb-3"><div class="col-12"><h2>Chronik</h2></div></div>';
$o .= '<div class="row"><div class="col-12"><div id="timeline">';



// WP_Query arguments
$args = array(
	'post_type'              => array( 'timeline' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '100',
	'orderby'           =>  'menu_order',
	'order'				=> 'ASC'
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

    $img = get_field('timeline_img');
    $year = get_field('timeline_year');
    $text = get_field('timeline_text');
    $alt = $img['alt'];
    $size = 'iph-timeline';
    $img_url = wp_get_attachment_image_url( $img['id'], $size );


    $o .= '
           <div class="item">
           <div class="timeline"></div>
					 <div class="timeline-bg"></div>
					 <div class="img-holder">';

		if (!empty($img)) {
			  $o .= '<img src="'.$img_url.'" alt="'.$alt.'"/>';
		}

    $o .= ' </div>  <span class="year d-block w-100 mb-2">'.$year.'</span>
           <span class="desc d-block">'.$text.'</span>


          </div>';

	}
} else {
	$o .= 'Keine Beiträge vorhanden';
}

// Restore original Post Data
wp_reset_postdata();







$o .='</div></div></div></section>';
$o.='<!-- SECTION TIMELINE // END-->';


echo $o;

?>
