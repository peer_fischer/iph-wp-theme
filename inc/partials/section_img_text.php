<?php

$o ='';
$inline_css = '';
$btn_text = '';



$headline = get_sub_field('section_img_text_headline');
$img_position = get_sub_field('section_img_text_img_pos');
$btn_text = get_sub_field('section_img_text_btn_text');
$btn_link = get_sub_field('section_img_text_btn_link');
$bg = get_sub_field('section_img_text_img_bg');


$o .='<!-- SECTION IMG_TEXT // START-->';
$o .='<section class="s-img-text" id="" style="">';
$o .= '<div class="container">';


$o .= '<div class="row pb-2 pt-2 mt-m-0 pb-sm-1 pt-md-3 pt-lg-4 pb-lg-4"><div class="col-8 col-md-8"><h2>'.$headline.'</h2></div><div class="d-none d-sm-block col-4 col-md-4 ">';

if (!empty($btn_text)) {

  $o.= '<a href="'.$btn_link.'" class="btn-primary mb-2 float-right">'.$btn_text.'</a>';

}

$o.= '</div></div>';

if ($img_position == 'left') {

  $img = get_sub_field('section_img_text_img_left');
  $alt = $img['alt'];
  $size = 'iph-size-m';
  $img_url = wp_get_attachment_image_url( $img['id'], $size );

  $text = get_sub_field('section_img_text_text_right');

  $o .= '<div class="row">
         <div class="col-12 col-md-7">
   <div class="wrapper">


            <img class="img-fluid lazyload" src="'.$img_url.'" alt="'.$alt.'" />';
            if ($bg == 1) {

              $o .= '<div class="bg"></div>';

            }

  $o.= '   </div></div>
        <div class="col-12 offset-0 col-md-4 pl-3 pl-md-0 offset-md-1">
           <div class="content mt-5">
           '.$text.'
           </div>';
           if (!empty($btn_text)) {

             $o.= '<a href="'.$btn_link.'" class="d-block d-sm-none btn-primary mb-2 mt-3 float-left">'.$btn_text.'</a>';

           }

  $o .=  ' </div>
  </div>';


} else {


  $img = get_sub_field('section_img_text_img_right');
  $alt = $img['alt'];
  $size = 'iph-size-m';
  $img_url = wp_get_attachment_image_url( $img['id'], $size );

  $text = get_sub_field('section_img_text_text_left');

  $o .= '<div class="row">
         <div class="col-12 col-md-4">
   <div class="wrapper">
            <img class="img-fluid lazyload" src="'.$img_url.'" alt="'.$alt.'" />';
            if ($bg == 1) {

              $o .= '<div class="bg"></div>';

            }

  $o.= '</div></div>
        <div class="col-12 offset-0 col-md-7 offset-md-1">
           <div class="content mt-5">
           '.$text.'
           </div>';

           if (!empty($btn_text)) {

             $o.= '<a href="'.$btn_link.'" class="d-block d-sm-none btn-primary mb-2 mt-3 float-left">'.$btn_text.'</a>';

           }

   $o .=    ' </div>
  </div>';


}

$o .='</div></section>';
$o.='<!-- SECTION IMG_TEXT // END-->';


echo $o;

?>
