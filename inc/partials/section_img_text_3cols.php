<?php

$o ='';
$inline_css = '';
$btn_text = '';



$headline = get_sub_field('section_img_text_3cols_headline');
$btn_text = get_sub_field('section_img_text_btn_text');
$btn_link = get_sub_field('section_img_text_btn_link');
$bg = get_sub_field('section_img_text_3cols_bg');

$size = 'iph-size-m';

$img_left  = get_sub_field('section_img_text_3cols_leftcol_img');
$img_left_hover = get_sub_field('section_img_text_3cols_leftcol_img_hover');
$alt_left = $img_left['alt'];
$img_left_url = wp_get_attachment_image_url( $img_left['id'], $size );

$img_middle  = get_sub_field('section_img_text_3cols_middlecol_img');
$img_middle_hover = get_sub_field('section_img_text_3cols_middlecol_img_hover');
$alt_middle = $img_middle['alt'];
$img_middle_url = wp_get_attachment_image_url( $img_middle['id'], $size );

$img_right  = get_sub_field('section_img_text_3cols_rightcol_img');
$img_right_hover = get_sub_field('section_img_text_3cols_righcol_img_hover');
$alt_right = $img_right['alt'];
$img_right_url = wp_get_attachment_image_url( $img_right['id'], $size );

$col_left_text = get_sub_field('section_img_text_3cols_leftcol_text');
$col_middle_text = get_sub_field('section_img_text_3cols_middlecol_text');
$col_right_text = get_sub_field('section_img_text_3cols_righcol_text');




$o .='<!-- SECTION IMG_TEXT_3COL // START-->';
$o .='<section class="s-img-text-3cols">';
$o .= '<div class="container">';


$o .= '<div class="row pt-4 pb-4"><div class="bg-50-left"></div><div class="col-12 col-md-8"><h2>'.$headline.'</h2></div><div class="col-12 col-md-4 ">';

if (!empty($btn_text)) {

  $o.= '<a href="'.$btn_link.'" class="btn-primary float-left float-md-right">'.$btn_text.'</a>';

}





$o.= '</div></div>';


  $o .= '<div class="row"><div class="col-12"><div class="row no-margin">';
  $o .= '<div class="col-12 col-md-4 no-padding col-1 cols-item">';

  if (!empty($img_left_hover)) {

  $o .= ' <div class="info-wrapper"><div class="info">'.$img_left_hover.'</div></div>';
  }
  if (!empty($img_left)) {
    $o .= '<img class="lazyload w-100 img-fluid" src="'.$img_left_url.'" alt="'.$alt_left.'"/>';
  }

  if (!empty($col_left_text)) {
    $o .= '<span class="d-block pr-2">'.$col_left_text.'</span>';
  }

  $o .= '</div>';
  $o .= '<div class="col-12 col-md-4 no-padding col-2 cols-item">';
  if (!empty($img_middle_hover)) {

  $o .= ' <div class="info-wrapper"><div class="info">'.$img_middle_hover.'</div></div>';
  }

  if (!empty($img_middle)) {
    $o .= '<img class="lazyload w-100 img-fluid" src="'.$img_middle_url.'" alt="'.$alt_middle.'"/>';
  }
  if (!empty($col_middle_text)) {
    $o .= '<span class="d-block pr-2">'.$col_middle_text.'</span>';
  }

  $o .= '</div>';
  $o .= '<div class="col-12  col-md-4 no-padding col-3 cols-item">';

  if (!empty($img_right_hover)) {

  $o .= ' <div class="info-wrapper"><div class="info">'.$img_right_hover.'</div></div>';
  }

  if (!empty($img_right)) {
    $o .= '<img class="lazyload w-100 img-fluid" src="'.$img_right_url.'" alt="'.$alt_right.'"/>';
  }
  if (!empty($col_right_text)) {
    $o .= '<span>'.$col_right_text.'</span>';
  }

  $o .= '</div>';
  $o .= '</div></div></div>';


$o .= '</div></section>';
$o .= '<!-- SECTION IMG_TEXT_3COL // END-->';


echo $o;

?>
