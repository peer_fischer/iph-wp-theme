<?php

$o ='';
$inline_css = '';
$btn_text = '';
$headline = get_sub_field('section_team_headline');
$btn_link = get_sub_field('section_team_joblink');
$btn_text = get_sub_field('section_team_btn_text');




echo $link;



$o .='<!-- SECTION TEAM // START-->';
$o .='<section class="s-team" id="" style="">';
$o .= '<div class="container">';

$o .= '<div class="row pb-2 pt-2 mt-m-0 pb-sm-1 pt-md-3 pt-lg-4 pb-lg-4"><div class="col-8 col-md-8"><h2>'.$headline.'</h2></div><div class="d-none d-sm-block col-4 col-md-4 ">';



  $o.= '<a href="'.$btn_link.'" class="btn-primary mb-2 float-right">'.$btn_text.'</a>';



$o.= '</div></div>';

$o.= '<div class="row"><div class="col-12"><div class="row no-margin">';


// WP_Query arguments
$args = array(
	'post_type'              => array( 'team' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '100',
  'order' => 'ASC',
  'orderby' => 'menu_order',

);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

    $img = get_field('team_img');
    $alt = $img['alt'];
    $size = 'iph-masonry-h';
    $img_url = wp_get_attachment_image_url( $img['id'], $size );
    $name = get_field('team_name');
    $job = get_field('team_job');
    $division = get_field('team_division');


    //$text = wp_trim_words( $text, 40, '...' );
    $date = get_the_date();

    $o .= '<div class="col-12 col-md-4 no-padding member">';

    if (!empty($name)) {
            $o .= ' <div class="info-wrapper"><div class="info"><span class="title d-block w-100">'.$name.'</span><span class="subtitle d-block w-100">'.$job.'</span><span class="subtitle d-block w-100">'.$division.'</span></div></div>';
}
      $o .= '<img class="img-fluid lazyload" src="'.$img_url.'" alt="'.$alt.'" />

          </div>';

	}
} else {
	$o .= 'Keine Beiträge vorhanden';
}

// Restore original Post Data
wp_reset_postdata();







$o .='</div>';
$o.= '<a href="'.$btn_link.'" class="d-block d-sm-none btn-primary mb-2 mt-3 float-left">'.$btn_text.'</a>';
$o .= '</div></div></div></section>';
$o.='<!-- SECTION TEAM // END-->';


echo $o;

?>
