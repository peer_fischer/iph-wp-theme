<?php

$o ='';
$inline_css = '';
$btn_text = '';



$headline = get_sub_field('section_vacancy_headline');
$headline_markup = get_sub_field('section_text_headline_markup');
$bg = get_sub_field('section_text_bg');
$btn_text = get_sub_field('section_vacancy_btn_text');
$btn_link = get_sub_field('section_vacancy_btn_link');
$content_task = get_sub_field('section_vacancy_text_tasks');
$content_expectation = get_sub_field('section_vacancy_text_expectation');
$content_requirement = get_sub_field('section_vacancy_text_requirements');
$bg = get_sub_field('section_vacancy_bg');
$cols = '8';

$o .='<!-- SECTION VACANCY // START-->';
$o .='<section class="s-vacancy" id="" style="">';
$o .= '<div class="container">';


$o .= '<div class="row pt-4 pb-2"><div class="col-12 col-md-8"><h2>'.$headline.'</h2></div><div class="col-12 col-md-4 ">';

if (!empty($btn_text)) {

  $o.= '<a href="mailto:'.$btn_link.'?subject='.$headline.'" class="btn-primary float-left float-md-right">'.$btn_text.'</a>';

}

$o.= '</div></div>';

$o .= '<div class="row"><div class="max-h bg-50-'.$bg.'">

</div>
         <div class="col-12 col-md-'.$cols.' section-content"><h3 class="w-100">Ihre Aufgaben</h3>'.$content_task.'<h3 class="w-100 mt-5">Das erwartet Sie</h3>'.$content_expectation.'<h3 class="w-100 mt-5">Das erwarten wir</h3>'.$content_requirement.'</div>';


$o .='</div></section>';
$o.='<!-- SECTION TEXT // END-->';


echo $o;

?>
