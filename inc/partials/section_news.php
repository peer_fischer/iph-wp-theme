<?php

$o ='';
$inline_css = '';
$btn_text = '';



$headline = get_sub_field('section_news_headline');
$btn_text = get_sub_field('section_news_btn_text');
$btn_link = get_sub_field('section_news_btn_link');



$o .='<!-- SECTION NEWS // START-->';
$o .='<section class="s-news" id="" style="">';
$o .= '<div class="container">';

$o .= '<div class="row pb-2 mt-5 mt-m-0 pb-sm-1 pt-md-3 pb-lg-4"><div class="col-8 col-md-8"><h2>'.$headline.'</h2></div><div class="d-none d-sm-block col-4 col-md-4 ">';

if (!empty($btn_text)) {

  $o.= '<a href="'.get_permalink(58).'" class="btn-primary mb-2 float-right">'.$btn_text.'</a>';

}

$o.= '</div></div>';


// WP_Query arguments
$args = array(
	'post_type'              => array( 'post' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '1',
	'order'                  => 'DESC',
	'orderby'                => 'date',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

    $img = get_field('post_img');
    $headline = get_field('post_headline');
    $alt = $img['alt'];
    $size = 'iph-size-m';
    $img_url = wp_get_attachment_image_url( $img['id'], $size );
    $text = get_field('post_text');
    //$text = wp_trim_words( $text, 40, '...' );
    $date = get_the_date();

    $o .= '<div class="row">
           <div class="col-12 col-md-4 order-2 order-md-1">
           <div class="content mt-0 mt-md-5">
           <span class="posted-on d-block">'.$date.'</span><h3>'.$headline.'</h3>
           '.$text.' <span class="read-more d-block mt-2"><a href="'.get_permalink(58).'">Weiterlesen</a></span>
           </div>';

           if (!empty($btn_text)) {

             $o.= '<a href="'.get_permalink(58).'" class="btn-primary mt-3 d-block d-sm-none">'.$btn_text.'</a>';

           }

    $o .= '</div>
          <div class="col-12 col-md-7 offset-md-1 order-1 order-md-2 ">
          <img class="img-fluid pb-3 lazyload" src="'.$img_url.'" alt="'.$alt.'" />

          </div>
    </div>';

	}
} else {
	$o .= 'Keine Beiträge vorhanden';
}

// Restore original Post Data
wp_reset_postdata();







$o .='</div></section>';
$o.='<!-- SECTION NEWS // END-->';


echo $o;

?>
